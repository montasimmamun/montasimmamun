# 👋 Hi, I'm [Mohammad Montasim -Al- Mamun Shuvo ](https://montasimmamun.github.io/Meet-Montasim/) <img src="https://github.com/montasimmamun/montasimmamun/blob/master/Earth.gif" width="24px"> <img align="right" src="https://github.com/montasimmamun/montasimmamun/blob/master/equilizer.gif" width="80px">


![status](https://img.shields.io/badge/status-up-brightgreen) ![Gender](https://img.shields.io/badge/gender-%F0%9F%A4%B5-lightgrey) ![](https://img.shields.io/badge/Relationship-Single-red) ![](https://visitor-badge.glitch.me/badge?page_id=github.com/montasimmamun)

  
  <img align="right" alt="GIF" src="https://github.com/montasimmamun/montasimmamun/blob/master/male.gif" width="350px">
  
  
 ## 📧 Contact Me on Social Media
<p align="left">
  <a href= "https://github.com/montasimmamun/">
    <img src="https://img.icons8.com/material-outlined/30/000000/source-code.png"/>
  </a>
  <a href= "https://www.linkedin.com/in/montasimmamun/">
    <img src="https://img.icons8.com/material-outlined/30/000000/linkedin.png"/>
  </a>
  <a href= "https://twitter.com/montasimmamun">
    <img src="https://img.icons8.com/material-outlined/30/000000/twitter.png"/>
  </a>
  <a href= "https://montasimmamun.github.io/">
    <img src="https://img.icons8.com/material-outlined/30/000000/geography.png"/>
  </a>
  <a href="https://www.buymeacoffee.com/montasimmamun">
    <img src="https://img.icons8.com/material-outlined/30/000000/cafe.png"/>
  </a>
  <a href="https://www.youtube.com/c/montasimmamun">
    <img src="https://img.icons8.com/material-outlined/30/000000/youtube-play.png"/>
  </a>
  <a href="https://orcid.org/0000-0002-6715-9134">
    <img src="https://img.icons8.com/material-outlined/30/000000/camera-addon-identification.png"/>
  </a>
  <a href="https://montasimmamun.github.io/cv/Montasim's%20Software%20Developer%20Resume%20.pdf">
    <img src="https://img.icons8.com/material-outlined/30/000000/parse-from-clipboard.png"/>
  </a>
  <a href="mailto:montasimmamun@gmail.com">
    <img src="https://img.icons8.com/ios-glyphs/30/000000/physics.png"/>
  </a>
  <a href="https://medium.com/@montasimmamun">
    <img src="https://img.icons8.com/ios-filled/30/000000/medium-new.png"/>
  </a>
  <a href="https://stackoverflow.com/users/10429621/montasim">
    <img src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/stackoverflow.svg" height="30px" width="30px" />
  </a>
</p>
  

From a young age, I’ve always had a sense of motivation and passion for writing codes & developing software that drives me forward. Whether it’s exploring unique opportunities, learning additional skills, or meeting new people, I bring these values to every experience throughout my life on a personal and professional level. Currently, I do work with Android, Python & Web Development. I can be reached at +8801722815469 or montasimmamun@gmail.com. To learn more about me, keep exploring my site, or reach out directly. 


## 💻 Programming Languages :
<img src="https://img.shields.io/badge/python%20-%2314354C.svg?&style=for-the-badge&logo=python&logoColor=white"/> <img src="https://img.shields.io/badge/c%20-%2300599C.svg?&style=for-the-badge&logo=c&logoColor=white"/> <img src="https://img.shields.io/badge/c++%20-%2300599C.svg?&style=for-the-badge&logo=c%2B%2B&ogoColor=white"/> <img src="https://img.shields.io/badge/java-%23ED8B00.svg?&style=for-the-badge&logo=java&logoColor=white"/> 	<img src="https://img.shields.io/badge/html5%20-%23E34F26.svg?&style=for-the-badge&logo=html5&logoColor=white"/> <img src="https://img.shields.io/badge/css3%20-%231572B6.svg?&style=for-the-badge&logo=css3&logoColor=white"/> <img src ="https://img.shields.io/badge/sqllite-%2307405e.svg?&style=for-the-badge&logo=sqlite&logoColor=white"/> <img src="https://img.shields.io/badge/git%20-%23F05033.svg?&style=for-the-badge&logo=git&logoColor=white"/>
<br>


  <img align="right" alt="GIF" src="https://github.com/montasimmamun/montasimmamun/blob/master/coding-freak.gif" width="400px">
  
## 💥 Talking About Personal Stuffs :

- 👨🏽‍💻 I'm a Programmer, Developer, Designer and Learner!
- 🌱 I love to solve coding problem.
- 👯 I’m currently learning everything I find interesting.🤝
- 💬 Ask me about anything, I am happy to help.
- 📫 How to reach me: montasimmamun@gmail.com
- 📝[Resume](https://drive.google.com/file/d/1-_oRi4ZmhWlIgqkTbNSi8ep1IAjlwGH6/view?usp=sharing)

<br>

## 💦 My Github Stats :

<br>

<p align = "center">
  <img alt="Montasim's Github Stats" src = "https://github-readme-stats.montasimmamun.vercel.app/api?username=montasimmamun&show_icons=true&theme=radical&line_height=27">
  <img alt="Montasim's Github Stats" src = "https://github-readme-stats.montasimmamun.vercel.app/api/top-langs/?username=montasimmamun&hide=css,html&theme=tokyonight">
</p>


<details>
  <summary>:zap: Recent Github Activity</summary>
  
<!--START_SECTION:activity-->

<!--END_SECTION:activity-->

</details>

<details>
  <summary> 📺 Latest YouTube Videos</summary>
  
<!-- YOUTUBE:START -->

<!-- YOUTUBE:END -->

</details>

<details>
  <summary> 📕 Latest Blog Posts</summary>
  
<!-- BLOG-POST-LIST:START -->

<!-- BLOG-POST-LIST:END -->

</details>


<div align="center">
<img alt="Thanks for visiting me" width="100%" src="https://github.com/montasimmamun/montasimmamun/blob/master/dino.gif" />
  

<br>
<br>

<em><b><h3 align='center'><i>I love connecting with different people from around the world, so if you want to be my friend, feel free to reach out and introduce yourself (don’t just say hi, tell me about yourself) 😊 💜</b> :)</i></h3></em>


<br />

[website]: https://montasimcv.000webhostapp.com/
[spotify]: https://open.spotify.com/user/swyqyimdc12jajde4vpwd2x1b
